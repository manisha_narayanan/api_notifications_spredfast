class CreateNotifications < ActiveRecord::Migration[5.1]
  def change
    create_table :notifications do |t|
      t.datetime :date
      t.text :message
      t.string :account_name
      t.timestamps
    end
  end
end
