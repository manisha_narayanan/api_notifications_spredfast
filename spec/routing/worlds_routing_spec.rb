require "rails_helper"

RSpec.describe Api::V1::WordsController, type: :routing do
  describe "routing" do

    it "routes to #words" do
      expect(:get => "api/v1/wordcount").to route_to("api/v1/words#index")
    end
  end
end
