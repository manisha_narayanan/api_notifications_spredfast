FactoryGirl.define do
  factory :notification do
    date "2017-07-07"
    message "test message"
    account_name "test account"
  end
end
