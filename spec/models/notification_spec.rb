require 'rails_helper'

RSpec.describe Notification, type: :model do

  it "requires account_name" do
    notification = FactoryGirl.build(:notification, :account_name => "", :message => "test", :date => "08-09-2017")

    expect(notification).to_not be_valid
  end

  it "requires message" do
    notification = FactoryGirl.build(:notification, :account_name => "test", :message => "", :date => "08-09-2017")

    expect(notification).to_not be_valid
  end

  it "requires date" do
    notification = FactoryGirl.build(:notification, :account_name => "test", :message => "test", :date => "")

    expect(notification).to_not be_valid
  end

  it "requires date to be a valid date" do
    notification = FactoryGirl.build(:notification, :account_name => "test", :message => "test", :date => "lksjdflsd")
    expect(notification).to_not be_valid
  end

  it "getWordCount should get correct world count" do
    word = "test"
    account_name = "party account"
    notification1 = FactoryGirl.create(:notification, :account_name => account_name, :message => "test message", :date => "08-09-2017")
    notification2 = FactoryGirl.create(:notification, :account_name => account_name, :message => "test this message", :date => "08-09-2017")

    notification3 = FactoryGirl.create(:notification, :account_name => account_name, :message => "test this message test", :date => "08-09-2017")
    Notification.getWordCount(word, account_name).should eq(4)
  end

  it "getWordCount should not include other user's words" do
    word = "test"
    account_name = "party account"

    notification1 = FactoryGirl.create(:notification, :account_name => account_name, :message => "test message", :date => "08-09-2017")
    notification2 = FactoryGirl.create(:notification, :account_name => "Another", :message => "test this message", :date => "08-09-2017")
    notification3 = FactoryGirl.create(:notification, :account_name => account_name, :message => "test this message test", :date => "08-09-2017")
    Notification.getWordCount(word, account_name).should eq(3)
  end

  it "getWordCount should not include substrings as words" do
    word = "test"
    account_name = "party account"
    notification1 = FactoryGirl.create(:notification, :account_name => account_name, :message => "test message", :date => "08-09-2017")
    notification3 = FactoryGirl.create(:notification, :account_name => account_name, :message => "testing this message test", :date => "08-09-2017")
    Notification.getWordCount(word, account_name).should eq(2)
  end

end
