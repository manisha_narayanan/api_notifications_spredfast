
Rails.application.routes.draw do
  namespace 'api' do
    namespace "v1" do
      resources :notifications, only: [:index, :show, :create]
      get 'wordcount' => "words#index"
    end
  end
end
#Rails.application.routes.draw do
#  resources :accounts

#  resources :notifications
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
#end
