# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

Ruby version 2.3
Rails version 5.1.4

- Within repo, run 'bundle install'
- Run rake db:migrate
- Run 'bundle exec rails server'
- Navigate to localhost:3000 in browser.
- relevent URLs are api/v1/notifications and api/v1/wordcount
- run bundle exec rspect to run unit tests
