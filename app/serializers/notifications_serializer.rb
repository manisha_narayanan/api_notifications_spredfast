class NotificationsSerializer < ActiveModel::Serializer
  attributes :id, :message, :date
end
