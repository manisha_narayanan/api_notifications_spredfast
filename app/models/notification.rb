class Notification < ApplicationRecord
include Filterable

validates :account_name, presence: true
validates :message, presence: true
validates :date, presence: true
validates_datetime :date

scope :start_date, -> (start_date) { where("date >= ?" , "#{start_date}%")}
scope :end_date, -> (end_date) {where("date < ?" , "#{end_date}%")}
scope :account_name, -> (account_name){ where(account_name: account_name) }
scope :keyword, -> (keyword){ where("message like ?", "%#{keyword}%")}


def self.getWordCount(word, account_name)
  count = 0;
  messages = Notification.where(account_name: account_name).pluck(:message)
  combined = messages.join(" ")
  padded_string = combined.center(combined.length + 2)
  padded_word = word.center(word.length + 2)
  count = padded_string.scan(padded_word).count
  count
end

end
