
  class Api::V1::WordsController < ApplicationController
    before_action :set_notification, only: [:show, :update, :destroy]

    # GET /notifications
    def index
      result = {:count => 0}
      if(!params[:account_name].nil? && !params[:word].nil?)
        result[:count] = Notification.getWordCount(params[:word], params[:account_name])
      end
      render json: result
    end
  end
